import AJV, {JSONSchemaType} from 'ajv'

export type AppConfig = {
    timestamp: boolean
    logCli: boolean,
    port: number
    logfile: string | null
}

const appConfigSchema: JSONSchemaType<AppConfig> = {
    type: 'object',
    properties: {
        timestamp: {
            type: 'boolean'
        },
        logCli: {
            type: 'boolean',
        },
        port: {
            type: 'integer'
        },
        logfile: {
            type: 'string',
            nullable: true
        }
    },
    required: [
        'timestamp',
        'logCli',
        'logfile',
        'port'
    ],
    additionalProperties: true
}

const appArgs = {
    logfile: process.env.LOG_FILE || null,
    logCli: !!(process.env.LOG_CLI) || false,
    port: parseInt(process.env.PORT || '') || 3000,
    timestamp: !!(process.env.LOG_TMSTAMP) || false
}

const validate = new AJV({removeAdditional: 'all'})
    .compile(appConfigSchema)

let _config: AppConfig | undefined

export default (): AppConfig => {
    if (_config) return _config

    const validateResult = validate(appArgs)

    if (!validateResult)
    {
        console.log(validate.errors)
        throw new Error('Application parameters validation error')
    }

    _config = appArgs as unknown as AppConfig

    return _config
}
