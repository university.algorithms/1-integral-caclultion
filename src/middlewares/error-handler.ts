import {ErrorRequestHandler} from 'express'
import GeneralErrorDTO from '@src/modules/message/general/error/dto'
import {GeneralType} from '@src/modules/message/general/types'
import WritersManager from '@src/modules/writers'


const centralErrorHandler: ErrorRequestHandler = (err, req, res, next) => {
    const errorDto = new GeneralErrorDTO({type: GeneralType.ERROR},{
        reason: (err as Error).message
    })

    WritersManager.write(JSON.stringify(err))

    res.status(400).json(errorDto)
}

export default centralErrorHandler
