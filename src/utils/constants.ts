export default {
    'BASE_URL': '',
    'SIMPSON_METHOD_API': '/api/simpson',
    'MONTE_CARLO_METHOD_API': '/api/monte-carlo'
}
