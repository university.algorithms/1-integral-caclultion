import * as mathjs from 'mathjs'

export type Extremum = {
    x: number,
    y: number
}

export type MeasuringSegment = {
    start: number,
    end: number
}

export type FunctionExtremum = {
    upper: Extremum,
    lower: Extremum
}

const getExtremes = (source: string, segments: MeasuringSegment, delta: number, initialStep: number = delta * 100): FunctionExtremum => {
    const equation = mathjs.parse(source)
    //const derive = mathjs.derivative(equation, 'x')

    /* ============================
    *   Calculating upper extremum
    *  ============================
    * */

    let startUpperPoint = segments.start
    let startUpperPointValue = -Infinity

    // Searching start evaluating point
    for (let currentX = segments.start; currentX < segments.end; currentX+= initialStep){
        const currentValue = equation.evaluate({x: currentX})
        if (currentValue > startUpperPointValue){
            startUpperPoint = currentX
            startUpperPointValue = currentValue
        }
    }

    let step = initialStep / 10
    let extremum = -Infinity
    let lastValue = -Infinity

    // Prepare initial point
    if (startUpperPoint - initialStep > segments.start)
        startUpperPoint -= initialStep

    let currentX

    for (currentX = startUpperPoint; currentX <= segments.end; currentX += step) {
        const currentValue = equation.evaluate({x: currentX})

        if (currentValue - lastValue < delta){
            break
        }

        if (currentValue > extremum){
            extremum = currentValue
            lastValue = currentValue
            continue
        }
        if (currentValue <= lastValue){
            currentX = currentX - (step * 2)
            // If start point located at descending
            if (currentX < segments.start) currentX = segments.start

            step = step / 10
            lastValue = currentValue
            continue
        }

        throw new Error('Unknown loop execution case')
    }

    const upperExtremumX = currentX
    const upperExtremumValue = extremum

    /* ============================
    *   Calculating lower extremum
    *  ============================
    * */

    let startLowerPoint = segments.start
    let startLowerPointValue = +Infinity

    // Searching start evaluating point
    for (let currentX = segments.start; currentX < segments.end; currentX += initialStep){
        const currentValue = equation.evaluate({x: currentX})
        if (currentValue < startLowerPointValue){
            startLowerPoint = currentX
            startLowerPointValue = currentValue
        }
    }

    step = initialStep / 10

    extremum = +Infinity
    lastValue = +Infinity

    for (currentX = startLowerPoint; currentX <= segments.end; currentX += step) {
        const currentValue = equation.evaluate({x: currentX})

        if (lastValue - currentValue < delta){
            break
        }

        if (currentValue < extremum){
            extremum = currentValue
            lastValue = currentValue
            continue
        }
        if (currentValue >= lastValue){
            currentX = currentX - (step * 2)
            // If start point located at descending
            if (currentX < segments.start){
                currentX = segments.start
            }

            step = step / 10
            lastValue = currentValue
            continue
        }

        throw new Error('Unknown loop execution case')
    }

    const lowerExtremumX = currentX
    const lowerExtremumValue = extremum

    return {
        lower: {
            x: lowerExtremumX,
            y: lowerExtremumValue
        },
        upper: {
            x: upperExtremumX,
            y: upperExtremumValue
        }
    }
}

export interface InputData{
    equation: string
    segments: {
        start: number
        end: number
    }
    accuracy: number
    periodicalInitStep: number | null
}

export default (data: InputData): FunctionExtremum => {

    const {
        start,
        end
    } = data.segments

    const {
        equation,
        accuracy,
        periodicalInitStep
    } = data

    return getExtremes(
        equation,
        {start, end},
        1 / Math.pow(10, accuracy),
        periodicalInitStep || undefined
    )
}
