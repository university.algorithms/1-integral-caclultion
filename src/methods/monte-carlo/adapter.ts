import {MethodData} from './index'
import {CalculationRequestMonteCarloDTO} from '@src/modules/message/calculation/request/monte-carlo/dto'

export default (dto: CalculationRequestMonteCarloDTO): MethodData => {
    return {
        equation: dto.parameters.equation,
        segments: dto.parameters.segment,
        throwsCount: dto.parameters.throwsCount,
        accuracy: dto.parameters.accuracy,
        measuringErrorRepeats: dto.parameters.measurementErrorRepeats,
        periodicalInitStep: dto.parameters.periodicalInitStep
    }
}
