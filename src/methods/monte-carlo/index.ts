import _ from 'lodash'
import chalk from 'chalk'
import * as mathjs from 'mathjs'
import writersManager from '@src/modules/writers'

import getExtremums from './extremums'
import evaluate, {EvaluatingResult} from './evaluate'

export interface MethodData {
    equation: string
    segments: {
        start: number
        end: number
    },
    throwsCount: number
    measuringErrorRepeats: number
    accuracy: number
    periodicalInitStep: number | null
}

export interface MethodResult extends EvaluatingResult {
    measurementError: number
}

export default async (data: MethodData): Promise<MethodResult> => {

    writersManager.write(chalk.cyanBright('Monte-Carlo method is chosen'))

    const extremums = getExtremums({
        equation: data.equation,
        segments: data.segments,
        accuracy: data.accuracy,
        periodicalInitStep: data.periodicalInitStep
    })

    writersManager.write(chalk.blueBright(
        `Evaluating [${data.equation}] with next params:\n` +
        `On interval [${data.segments.start}; ${data.segments.end}]\n` +
        `With lower[${extremums.lower.y}] and upper border[${extremums.upper.y}];\n` +
        `With throws count: ${data.throwsCount};\n` +
        `Measurement steps: ${data.measuringErrorRepeats}`
    ))

    writersManager.write('Evaluating started...')

    const equation = mathjs.parse(data.equation).compile()

    const evaluatingParameters = {
        equation,
        segments: data.segments,
        throwsCount: data.throwsCount,
        extremums
    }

    const result = evaluate(evaluatingParameters)

    writersManager.write('Started collecting of data for measurement error calculating...')

    const sequence = _.range(data.measuringErrorRepeats).map(() =>{
        const result = evaluate(evaluatingParameters)
        return result.numericIntegral
    })

    const averageSqr = sequence
        .map(v => v * v)
        .reduce((acc, v) => acc + v, 0)

    const average =  sequence.reduce((acc, v) => acc + v, 0) / sequence.length

    const measurementError = Math.sqrt(
        ((averageSqr) / sequence.length) - (average * average)
    )

    writersManager.write(chalk.greenBright(
        'Result:\n' +
        `Shared area: ${result.sharedArea};\n` +
        `Numeric integral value: ${result.numericIntegral.toFixed(data.accuracy)} | %${(Math.abs(result.numericIntegral  / result.sharedArea * 100)).toFixed()};\n` +
        `Measurement error: ${measurementError};\n` +
        // Margin
        '\n\n\n'
    ))

    return  {
        ...result,
        measurementError
    }
}
