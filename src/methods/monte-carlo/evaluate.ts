import {FunctionExtremum} from '@src/methods/monte-carlo/extremums'
import _ from 'lodash'
import random from '@src/utils/random'
import {EvalFunction} from 'mathjs'

export type EvaluatingData = {
    equation: EvalFunction,
    extremums: FunctionExtremum
    segments: {
        start: number,
        end: number
    },
    throwsCount: number
}

export type Point = {
    x: number,
    y: number,
    isHit: boolean
}

export interface EvaluatingResult {
    numericIntegral: number
    sharedArea: number
    points: Array<Point>
    shapeBorders: {
        lower: number,
        upper: number
    }
}

export default (data: EvaluatingData): EvaluatingResult => {

    const {
        start,
        end
    } = data.segments

    const {
        extremums,
        throwsCount,
        equation
    } = data

    if (extremums.lower.y > 0)
        extremums.lower.y = 0

    if (extremums.upper.y < 0)
        extremums.upper.y = 0

    const sharedArea = (end - start) * (extremums.upper.y - extremums.lower.y)

    let thrownInArea = 0

    const points: Array<Point> = _.range(throwsCount)
        .map(() => {
            if (extremums === undefined)
                throw new Error('Unknown typescript error')

            const rndX = random(start, end)
            const rndY = random(extremums.lower.y, extremums.upper.y)

            const integralY = equation.evaluate({x: rndX})

            const point: Point = {
                x: rndX,
                y: rndY,
                isHit: false
            }


            if
            (
                integralY > 0 &&
                rndY > 0 &&
                rndY < integralY
            ){
                ++thrownInArea
                point.isHit = true
            }

            if
            (
                integralY < 0 &&
                rndY < 0 &&
                rndY > integralY
            ){
                --thrownInArea
                point.isHit = true
            }

            return point
        })

    const integralPercent = thrownInArea / data.throwsCount
    const integralValue = integralPercent * sharedArea

    return {
        points,
        sharedArea,
        shapeBorders: {
            upper: extremums.upper.y,
            lower: extremums.lower.y
        },
        numericIntegral: integralValue
    }
}
