import * as math from 'mathjs'
import _ from 'lodash'

export type EvaluatingData = {
    equation: string,
    segment: {
        start: number,
        end: number
    },
    segmentsCount: number
}

type Point = {
    coefficient: number
    x: number
    y: number
}

export interface EvaluatingResult {
    points: Array<Point>
    numericResult: number
}

export default async (data: EvaluatingData): Promise<EvaluatingResult> => {

    const {
        start,
        end,
    } = data.segment

    const {
        equation,
        segmentsCount
    } = data

    if (segmentsCount % 2)
        throw new Error('Counts size needs to be even')

    const segmentLength = end - start

    const step = segmentLength / segmentsCount

    const evaluateEquation = math.parse(equation).compile().evaluate

    const points: Array<Point> = []

    _.range(0, segmentsCount + 1).forEach(stepIndex => {
        const currentX = start + (step * stepIndex)
        const currentY = evaluateEquation({
            x: currentX
        }) as number

        points.push({
            x: currentX,
            y: currentY,
            coefficient: 1
        })
    })


    _.range(1, points.length - 1)
        .filter(index => {
            if (index % 2)
                points[index].coefficient = 4
            else
                points[index].coefficient = 2
        })

    const k4Values = points.filter(point => point.coefficient === 4)

    const k2Values = points.filter(point => point.coefficient === 2)

    const sum =
        (points[0].y) +
        (4 * (k4Values.reduce((acc, v) => acc + v.y, 0))) +
        (2 * (k2Values.reduce((acc, v) => acc + v.y, 0))) +
        (points[points.length - 1].y)

    const result = (end - start) / (3 * segmentsCount) * (sum)

    return {
        points,
        numericResult: result
    }
}
