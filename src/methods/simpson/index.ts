import _ from 'lodash'
import chalk from 'chalk'
import writersManager from '@src/modules/writers'
import evaluate, {EvaluatingResult} from './evaluate'

export interface MethodResult extends EvaluatingResult {
    measurementError: number
}

export interface MethodData {
    equation: string,
    segment: {
        start: number
        end: number
    }
    segmentsCount: number
}

export default async (data: MethodData): Promise<MethodResult> => {

    const originalSettings = _.cloneDeep(data)

    writersManager.write(chalk.cyanBright('Simpson method is chosen\n'))

    writersManager.write(chalk.blueBright(
        `Evaluating equation: ${data.equation}\n` +
        `On [${data.segment.start}; ${data.segment.end}] with ${data.segmentsCount} segment parts\n`
    ))

    writersManager.write(chalk.blueBright('Evaluating integral...'))

    const originalResult = await evaluate(originalSettings)

    writersManager.write(
        originalResult.points.map(point => `[${point.x};${point.y}] k${point.coefficient}`).join('\n')
    )


    const doubledSettings = _.cloneDeep(data)
    doubledSettings.segmentsCount += doubledSettings.segmentsCount

    writersManager.write(chalk.blueBright('Evaluating measurement error...'))

    const doubledResult = await evaluate(doubledSettings)

    const measurementError = (1 / 15) * (doubledResult.numericResult - originalResult.numericResult)

    writersManager.write(chalk.greenBright(
        `Result: ${originalResult.numericResult}\n` +
        `Measurement error: ${measurementError}`
    ))

    return {
        ...originalResult,
        measurementError
    }
}
