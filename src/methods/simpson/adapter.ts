import {CalculationRequestSimpsonDTO} from '@src/modules/message/calculation/request/simpson/dto'
import {MethodData} from './index'

export default (dto: CalculationRequestSimpsonDTO): MethodData => {
    return {
        segment: dto.parameters.segment,
        segmentsCount: dto.parameters.segmentsCount,
        equation: dto.parameters.equation
    }
}
