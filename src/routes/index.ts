import {Router} from 'express'
import constants from '@src/utils/constants'

const router = Router()

router.get('/', (req, res) => {
    res.render('index', {
        ...constants
    })
})

export default router
