import {Router} from 'express'
import {CalculationRequestSimpsonDTO} from '@src/modules/message/calculation/request/simpson/dto'
import evalMethod from '@src/methods/simpson'
import inputAdapter from '@src/methods/simpson/adapter'
import {CalculationResponseSimpsonDTO} from '@src/modules/message/calculation/response/simpson/dto'

const router = Router()

router.post('/simpson', async (req, res, next) => {

    try {
        const requestBody = req.body

        const dto = CalculationRequestSimpsonDTO.unpack(requestBody)

        const dataAdapter = inputAdapter(dto)

        const methodResult = await evalMethod(dataAdapter)

        const response = new CalculationResponseSimpsonDTO({
            segmentsCount: dto.parameters.segmentsCount,
            segment: dto.parameters.segment,
            equation: dto.parameters.equation,
            evaluatingError: methodResult.measurementError,
        }, {
            points: methodResult.points,
            numericResult: methodResult.numericResult
        })

        res.json(response)
    }
    catch (e) {
        next(e)
    }
})

export default router
