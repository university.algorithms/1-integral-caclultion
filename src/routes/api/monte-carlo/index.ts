import {Router} from 'express'
import inputAdapter from '@src/methods/monte-carlo/adapter'
import method from '@src/methods/monte-carlo'
import {CalculationRequestMonteCarloDTO} from '@src/modules/message/calculation/request/monte-carlo/dto'
import {CalculationResponseMonteCarloDTO} from '@src/modules/message/calculation/response/monte-carlo/dto'

const router = Router()

router.post('/monte-carlo', async (req, res, next) => {
    try {
        const requestBody = req.body

        const dto = CalculationRequestMonteCarloDTO.unpack(requestBody)

        const dataAdapter = inputAdapter(dto)

        const methodResult = await method(dataAdapter)

        const additionalData = dto.headers.receivePoints ? { points: methodResult.points} : {}

        const response = new CalculationResponseMonteCarloDTO({
            area: {
                start: dto.parameters.segment.start,
                end: dto.parameters.segment.end,
                top: methodResult.shapeBorders.upper,
                bottom: methodResult.shapeBorders.lower
            },
            throwsCount: dto.parameters.throwsCount,
            equation: dto.parameters.equation
        },{
            sharedArea: methodResult.sharedArea,
            numericResult: methodResult.numericIntegral,
            measurementError: methodResult.measurementError,
            ...additionalData
        })

        res.json(response)
    }
    catch (e) {
        next(e)
    }
})

export default router
