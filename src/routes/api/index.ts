import {Router, json} from 'express'
import monteCarlo from './monte-carlo'
import simpson from './simpson'

const router = Router()

router.use('/', json(), monteCarlo, simpson)

export default router
