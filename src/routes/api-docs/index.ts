import swagger from '@src/modules/swagger'
import router from '@src/routes/api'

router.use('/', swagger)

export default router
