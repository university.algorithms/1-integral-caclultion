import {join} from 'path'

import express from 'express'
import cors from 'cors'

import getConfig from '@src/config'

import swagger from './routes/api-docs'
import apiRoute from './routes/api'
import viewRoute from './routes/index'

import errorHandler from './middlewares/error-handler'
import WritersManager from '@src/modules/writers'
import LogWriter from '@src/modules/writers/log-writer'
import CLIWriter from '@src/modules/writers/cli-writer'

const config = getConfig()

const app = express()

if (config.logfile)
    WritersManager.add(new LogWriter(config.logfile, undefined, config.timestamp))

if (config.logCli)
    WritersManager.add(new CLIWriter())

app.use(cors({
    origin: '*'
}))

app.set('view engine', 'ejs')
app.set('views', join(__dirname, '/views'))


app.use('/api', apiRoute)

app.use('/api-docs', swagger)

app.use('/', viewRoute)

app.use(errorHandler)

app.all('/*', (req, res)=> res.redirect('/'))

app.listen(config.port)
