import fs from 'fs'
import stripColor from 'strip-color'
import {Writer} from './index'

export default class LogWriter implements Writer{
    private buffer: Array<string> = []
    private timeoutHandler: NodeJS.Timeout | null = null

    constructor(private readonly filepath: string, private readonly flushTimeout: number = 300, private readonly timestamp: boolean = true) {}

    write(data: string): void{
        if (this.timeoutHandler === null){
            this.timeoutHandler = setTimeout(this.writeHandler.bind(this), this.flushTimeout)
        }
        if (this.timestamp){
            const timestamp = new Date().toLocaleTimeString('ru-RU', {hour12: false})
            data = `[${timestamp}; ${process.hrtime()}]\n` + data + '\n'
        }
        this.buffer.push(stripColor(data))
    }

    private writeHandler(){
        fs.promises.writeFile(this.filepath, this.buffer.join('\n'), {flag: 'a'})
        this.timeoutHandler = null
    }
}
