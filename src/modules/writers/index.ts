export interface Writer {
    write(data: string): void
}

export class WritersManager {
    constructor(private writers: Array<Writer> = []) { }

    add(inst: Writer): void {
        this.writers.push(inst)
    }

    write(data: string): void
    write(data: Record<string, unknown>): void
    write(data: unknown): void {
        const toWrite = typeof data === 'string' ? data : JSON.stringify(data)
        this.writers.forEach(w => w.write(toWrite))
    }
}

// Singleton
export default new WritersManager()
