import {Writer} from './index'

export default class CLIWriter implements Writer {
    private buffer: Array<string> = []
    private timeoutHandler: NodeJS.Timeout | null = null

    constructor(private readonly flushTimeout: number = 300) {}

    write(data: string): void{
        if (this.timeoutHandler !== null){
            this.timeoutHandler = setTimeout(this.writeHandler.bind(this), this.flushTimeout)
        }
        this.buffer.push(data)
    }

    private writeHandler(){
        console.log(this.buffer.join('\n'))
        this.timeoutHandler = null
    }
}
