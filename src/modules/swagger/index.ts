import {Router} from 'express'
import swaggerUi from 'swagger-ui-express'

// eslint-disable-next-line @typescript-eslint/no-var-requires
const docs = require('./openapi.json')

const router = Router()

router.use('/', swaggerUi.serve, swaggerUi.setup(docs))

export default router
