import {General, GeneralHeaders, GeneralPayload} from './types'

export abstract class GeneralDTO implements General {
    abstract headers: GeneralHeaders
    abstract payload: GeneralPayload

    pack(): string {
        return JSON.stringify(this)
    }
}
