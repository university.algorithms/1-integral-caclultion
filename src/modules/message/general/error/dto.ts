import {GeneralDTO} from '@src/modules/message/general/dto'
import {GeneralError, GeneralErrorHeaders, GeneralErrorPayload} from '@src/modules/message/general/error/types'

export default class GeneralErrorDTO extends GeneralDTO implements GeneralError {
    constructor(public readonly headers: GeneralErrorHeaders, public readonly payload: GeneralErrorPayload) {
        super()
    }
}
