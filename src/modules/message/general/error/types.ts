import {General, GeneralHeaders, GeneralPayload, GeneralType} from '../types'

export interface GeneralError extends General{
    headers: GeneralErrorHeaders
    payload: GeneralErrorPayload
}

export interface GeneralErrorHeaders extends GeneralHeaders{
    type: GeneralType.ERROR
}

export interface GeneralErrorPayload extends GeneralPayload {
    reason: string
}
