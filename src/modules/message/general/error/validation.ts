import AJV,{JSONSchemaType} from 'ajv'
import {GeneralError} from '@src/modules/message/general/error/types'
import {GeneralType} from '@src/modules/message/general/types'

const schema: JSONSchemaType<GeneralError> = {
    type: 'object',
    properties: {
        headers: {
            type: 'object',
            properties: {
                type: {type: 'string', const: GeneralType.ERROR},
            },
            required: ['type']
        },
        payload: {
            type: 'object',
            properties: {
                reason: {type: 'string'},
            },
            required: []
        }
    },
    required: ['headers', 'payload']
}

export default new AJV().compile(schema)
