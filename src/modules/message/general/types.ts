export interface General {
    headers: GeneralHeaders,
    payload: GeneralPayload
}

export interface GeneralHeaders {
    type: GeneralType
}

export enum GeneralType {
    ERROR = 'ERROR'
}

// For correct hierarchy
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface GeneralPayload {}
