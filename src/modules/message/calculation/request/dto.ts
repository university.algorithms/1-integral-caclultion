import {CalculationRequest, CalculationRequestHeaders, CalculationRequestParameters} from './types'

export abstract class CalculationRequestDTO implements CalculationRequest {
    abstract parameters: CalculationRequestParameters
    abstract headers: CalculationRequestHeaders

    pack(): string {
        return JSON.stringify(this)
    }
}
