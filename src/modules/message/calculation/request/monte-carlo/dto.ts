import {CalculationRequestDTO} from '../dto'
import {
    CalculationRequestMonteCarlo,
    CalculationRequestMonteCarloHeaders,
    CalculationRequestMonteCarloParameters
} from './types'
import validate from './valiadtion'

export class CalculationRequestMonteCarloDTO extends CalculationRequestDTO implements CalculationRequestMonteCarlo {
    constructor(public readonly headers: CalculationRequestMonteCarloHeaders, public readonly parameters: CalculationRequestMonteCarloParameters) {
        super()
    }

    public static unpack(data: Record<string, unknown>): CalculationRequestMonteCarloDTO
    public static unpack(data: string): CalculationRequestMonteCarloDTO
    public static unpack(data: unknown): CalculationRequestMonteCarloDTO {

        let processedData

        if (typeof data === 'string')
            processedData = JSON.parse(data)
        else if (typeof data === 'object')
            processedData = data
        else
            throw Error('Invalid data type')

        const validationResult = validate(processedData)

        if (!validationResult)
            throw new Error('Validation error')

        const validated = processedData as CalculationRequestMonteCarlo

        return new CalculationRequestMonteCarloDTO(validated.headers, validated.parameters)
    }
}
