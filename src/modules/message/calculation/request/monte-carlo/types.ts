import {CalculationRequest, CalculationRequestHeaders, CalculationRequestParameters} from '../types'

export interface CalculationRequestMonteCarloHeaders extends CalculationRequestHeaders {
    receivePoints: boolean
}

export interface CalculationRequestMonteCarloParameters extends CalculationRequestParameters {
    equation: string
    segment: {
        start: number
        end: number
    }
    throwsCount: number
    accuracy: number
    measurementErrorRepeats: number
    periodicalInitStep: number | null
}

export interface CalculationRequestMonteCarlo extends CalculationRequest {
    headers: CalculationRequestMonteCarloHeaders
    parameters: CalculationRequestMonteCarloParameters
}
