import AJV, {JSONSchemaType, ValidateFunction} from 'ajv'
import {CalculationRequestMonteCarlo} from './types'

const schema: JSONSchemaType<CalculationRequestMonteCarlo> = {
    type: 'object',
    properties: {
        headers: {
            type: 'object',
            properties: {
                receivePoints: {
                    type: 'boolean'
                }
            },
            required: ['receivePoints']
        },
        parameters: {
            type: 'object',
            properties: {
                equation: {
                    type: 'string'
                },
                segment: {
                    type: 'object',
                    properties: {
                        start: {type: 'number'},
                        end: {type: 'number'}
                    },
                    required: ['start', 'end']
                },
                throwsCount: {
                    type: 'integer'
                },
                accuracy: {
                    type: 'integer'
                },
                periodicalInitStep: {
                    type: 'number',
                    nullable: true
                },
                measurementErrorRepeats: {
                    type: 'integer'
                }
            },
            required: ['equation', 'segment', 'throwsCount', 'accuracy', 'periodicalInitStep', 'measurementErrorRepeats']
        }
    },
    required: ['headers','parameters']
}

export default new AJV().compile(schema) as ValidateFunction<CalculationRequestMonteCarlo>
