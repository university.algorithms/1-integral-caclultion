import {CalculationRequest, CalculationRequestHeaders, CalculationRequestParameters} from '../types'

// For correct hierarchy
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface CalculationRequestHeadersSimpson extends CalculationRequestHeaders {}

export interface CalculationRequestSimpsonParameters extends CalculationRequestParameters {
    segment: {
        start: number
        end: number
    }
    segmentsCount: number
    equation: string
}

export interface CalculationRequestSimpson extends CalculationRequest {
    headers: CalculationRequestHeadersSimpson
    parameters: CalculationRequestSimpsonParameters
}
