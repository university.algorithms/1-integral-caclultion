import {CalculationRequestDTO} from '../dto'
import {CalculationRequestHeadersSimpson, CalculationRequestSimpson, CalculationRequestSimpsonParameters} from './types'
import validate from './validation'

export class CalculationRequestSimpsonDTO extends CalculationRequestDTO implements CalculationRequestSimpson {
    constructor(public readonly headers: CalculationRequestHeadersSimpson, public readonly parameters: CalculationRequestSimpsonParameters) {
        super()
    }

    public static unpack(data: Record<string, unknown>): CalculationRequestSimpsonDTO
    public static unpack(data: string): CalculationRequestSimpsonDTO
    public static unpack(data: unknown): CalculationRequestSimpsonDTO {

        let processedData

        if (typeof data === 'string')
            processedData = JSON.parse(data)
        else if (typeof data === 'object')
            processedData = data
        else
            throw Error('Invalid data type')

        const validationResult = validate(processedData)

        if (!validationResult)
            throw new Error('Validation error')

        const validated = processedData as CalculationRequestSimpson

        return new CalculationRequestSimpsonDTO(validated.headers, validated.parameters)
    }
}
