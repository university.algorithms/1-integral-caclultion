import AJV, {JSONSchemaType} from 'ajv'
import {CalculationRequestSimpson} from './types'

const schema: JSONSchemaType<CalculationRequestSimpson> = {
    type: 'object',
    properties: {
        headers:{
            type: 'object',
            properties: {},
            required: []
        },
        parameters: {
            type: 'object',
            properties: {
                segment: {
                    type: 'object',
                    properties: {
                        start: {type: 'number'},
                        end: {type: 'number'},
                    },
                    required: ['start', 'end']
                },
                segmentsCount: {
                    type: 'integer'
                },
                equation: {
                    type: 'string'
                }
            },
            required: ['segment', 'segmentsCount']
        }
    },
    required: ['headers', 'parameters']
}

export default new AJV().compile(schema)
