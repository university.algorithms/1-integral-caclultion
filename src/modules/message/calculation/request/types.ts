export interface CalculationRequest {
    headers: CalculationRequestHeaders
    parameters: CalculationRequestParameters
}

// For correct hierarchy
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface CalculationRequestParameters {}

// For correct hierarchy
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface CalculationRequestHeaders {}
