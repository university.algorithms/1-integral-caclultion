export enum CalculationMethod {
    Simpson = 'Simpson',
    MonteCarlo = 'MonteCarlo'
}
