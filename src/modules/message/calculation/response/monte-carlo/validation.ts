import AJV, {JSONSchemaType} from 'ajv'
import {CalculationResponseMonteCarlo} from './types'


const schema: JSONSchemaType<CalculationResponseMonteCarlo> = {
    type: 'object',
    properties: {
        headers: {
            type: 'object',
            properties: {
                throwsCount: {
                    type: 'integer'
                },
                area: {
                    type: 'object',
                    properties: {
                        start: {type: 'number'},
                        end: {type: 'number'},
                        top: {type: 'number'},
                        bottom: {type: 'number'}
                    },
                    required: ['start', 'end', 'top', 'bottom']
                },
                equation: {
                    type: 'string'
                }
            },
            required: ['throwsCount', 'area']
        },
        data: {
            type: 'object',
            properties: {
                points: {
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            x: {type: 'number'},
                            y: {type: 'number'},
                            isHit: {type: 'boolean'}
                        },
                        required: ['x', 'y', 'isHit']
                    },
                    nullable: true
                },
                sharedArea: {type: 'number'},
                numericResult: {type: 'number'},
                measurementError: {type: 'integer'}
            },
            required: ['sharedArea', 'numericResult', 'measurementError']
        }
    },
    required: ['headers', 'data']
}

export default new AJV({removeAdditional: 'all'}).compile(schema)
