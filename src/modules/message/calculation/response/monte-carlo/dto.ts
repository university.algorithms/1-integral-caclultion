import {CalculationResponseDTO} from '../dto'
import {
    CalculationResponseDataMonteCarlo,
    CalculationResponseHeadersMonteCarlo, CalculationResponseMonteCarlo
} from './types'


export class CalculationResponseMonteCarloDTO extends CalculationResponseDTO implements CalculationResponseMonteCarlo {
    constructor(public readonly headers: CalculationResponseHeadersMonteCarlo, public readonly data: CalculationResponseDataMonteCarlo) {
        super()
    }
}
