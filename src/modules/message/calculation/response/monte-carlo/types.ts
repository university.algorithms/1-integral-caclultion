import {
    CalculationResponse,
    CalculationResponseData,
    CalculationResponseHeaders
} from '../types'


export interface CalculationResponseMonteCarlo extends CalculationResponse {
    headers: CalculationResponseHeadersMonteCarlo,
    data: CalculationResponseDataMonteCarlo
}

export interface CalculationResponseHeadersMonteCarlo extends CalculationResponseHeaders {
    area: {
        start: number
        end: number
        top: number
        bottom: number
    }
    throwsCount: number
    equation: string
}

export interface CalculationResponseDataMonteCarlo extends CalculationResponseData {
    points?: Array<CalculationResponseDataMonteCarloIntegralEntry>
    sharedArea: number
    numericResult: number
    measurementError: number
}

export type CalculationResponseDataMonteCarloIntegralEntry = {
    isHit: boolean
    x: number
    y: number
}
