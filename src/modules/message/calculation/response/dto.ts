import {
    CalculationResponse,
    CalculationResponseData,
    CalculationResponseHeaders
} from './types'

export abstract class CalculationResponseDTO implements CalculationResponse {
    abstract headers: CalculationResponseHeaders
    abstract data: CalculationResponseData

    pack(): string {
        return JSON.stringify(this)
    }
}
