import AJV, {JSONSchemaType} from 'ajv'
import {CalculationResponseSimpson} from './types'

const schema: JSONSchemaType<CalculationResponseSimpson> = {
    type: 'object',
    properties: {
        headers: {
            type: 'object',
            properties: {
                segment: {
                    type: 'object',
                    properties: {
                        start: {
                            type: 'number'
                        },
                        end: {
                            type: 'number'
                        }
                    },
                    required: ['start', 'end']
                },
                segmentsCount: {
                    type: 'integer'
                },
                equation: {
                    type: 'string',
                    minLength: 1
                },
                evaluatingError: {
                    type: 'number'
                }
            },
            required: ['segment', 'segmentsCount', 'equation', 'evaluatingError']
        },
        data: {
            type: 'object',
            properties: {
                points: {
                    type: 'array',
                    items: {
                        type: 'object',
                        properties: {
                            x: {type: 'number'},
                            y: {type: 'number'},
                            coefficient: {type: 'integer'}
                        },
                        required: ['x', 'y', 'coefficient']
                    }
                },
                numericResult: {
                    type: 'number'
                }
            },
            required: ['points', 'numericResult']
        }
    },
    required: ['headers', 'data']
}

export default new AJV({removeAdditional: true}).compile(schema)
