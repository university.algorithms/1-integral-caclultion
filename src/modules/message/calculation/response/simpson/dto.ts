import {
    CalculationResponseDataSimpson,
    CalculationResponseHeadersSimpson, CalculationResponseSimpson
} from './types'
import {CalculationResponseDTO} from '../dto'


export class CalculationResponseSimpsonDTO extends CalculationResponseDTO implements CalculationResponseSimpson {
    constructor(public readonly headers: CalculationResponseHeadersSimpson, public readonly data: CalculationResponseDataSimpson) {
        super()
    }
}
