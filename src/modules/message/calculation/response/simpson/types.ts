import {
    CalculationResponse,
    CalculationResponseData,
    CalculationResponseHeaders
} from '../types'

export interface CalculationResponseSimpson extends CalculationResponse {
    headers: CalculationResponseHeadersSimpson,
    data: CalculationResponseDataSimpson
}

export interface CalculationResponseHeadersSimpson extends CalculationResponseHeaders {
    segment: {
        start: number
        end: number
    }
    segmentsCount: number
    equation: string
    evaluatingError: number
}

export interface CalculationResponseDataSimpson extends CalculationResponseData {
    points: Array<CalculationResponseDataSimpsonIntegralEntry>
    numericResult: number
}

export type CalculationResponseDataSimpsonIntegralEntry = {
    coefficient: number
    x: number
    y: number
}
