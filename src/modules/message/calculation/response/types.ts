export interface CalculationResponse {
    headers: CalculationResponseHeaders
    data: CalculationResponseData
}

// For correct hierarchy
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface CalculationResponseHeaders {}

// For correct hierarchy
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface CalculationResponseData {}
