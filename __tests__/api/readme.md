### How to run dot http files
All .http files can be executed with JetBrains's `rectcli` app.  
Source code can be founded on [this link](https://github.com/restcli/restcli)

