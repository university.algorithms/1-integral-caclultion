# Algorithms and evaluating methods.
## First laboratory work.


__Made by__: Alexey Moskalenko.  
__Group__: IVT-192

## Help

### Online version

[On Heroku](https://integral-calc.herokuapp.com/) 

#### Tools and installation

Required software:
- Node.js LTS & npm (included in node.js app bundle)

Installation:
```shell
npm install

# Without additional parameters
npm run start

# With logging
npm run start:log
```

#### Application start environment flags

- **PORT** - Port to bind application. _Int_ `Default: 3000`
- **LOG_FILE** - Path to log file. _String_ `Default: None.`
- **LOG_CLI** - Specifies whether to log to the console. _Any non-false value_ `Default: none`
- **LOG_TMSTAMP** - Whether it is necessary to indicate the time in the file logs. __Any non-false value_ `Default: false`


#### Paths & api

`/` [Root page] - UI with dynamic view model.  
`/api-docs` - OpenAPI specification with interaction examples.


#### Equation syntax

This application uses `math.js` to evaluate equation, so you need follow the above lib syntax.  
[Syntax docs](https://mathjs.org/docs/expressions/syntax.html)
